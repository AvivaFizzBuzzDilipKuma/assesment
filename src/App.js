import React from 'react';
import logo from './logo.svg';
import './App.css';
import Header from './components/header/header';
import TaskList from './components/tasklist/tasklist'; 
import Task from './components/task/task';  
import Footer from './components/footer/footer'; 

class FrizzBuzz extends React.Component {
  render() {
    return (
      <div>
        <Header/>
        <TaskList />
        <Task/>
        <Footer/>
      </div>
    );
  }
};
export default FrizzBuzz;
