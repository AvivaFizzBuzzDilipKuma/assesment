import React, { Component } from 'react';
import './header.css';
class Header extends Component {
    render() {
        return (
            <div>
            <header className="Header">
              <h1 className="Header-title">Fizz- Buzz</h1>
            </header>                   
            </div>
        );
    }
}
export default Header;