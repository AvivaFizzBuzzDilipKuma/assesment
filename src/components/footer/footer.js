import React, { Component } from 'react';
import './footer.css';


// Can also be written it as functional component i.e. as stateless component
// I am assuming state and some functionality,hence the stateful component
class Footer extends Component{
    render(){
        return (
            <div className="footer">
                <div className="copyRight">
                    Copyright 2019 Aviva | All Rights reserved
                </div>
            </div>
        );
    }
}

export default Footer;