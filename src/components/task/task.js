import React, { Component } from 'react';
import './task.css';
class Task extends Component {
    constructor(props) {
        super(props);
        this.state = {
          dayName:['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'][new Date().getDay()], 
          inputValue:'',
          errorTxt:'',
          numberList:[],
          index:1,
          totalPages:'',
          showButton:false,
          splitArray:[],
        };
      }
    //['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'][new Date().getDay()],
    inputChange= (event) => {
      let list=[];
      this.setState({inputValue:'',numberList:'',splitArray:'',index:1,showButton:false});
      if(event.target.value >0 && event.target.value <1001 ){
          for(let i=1;i<=event.target.value;i++){
             list.push(i)
          }
           this.setState({inputValue : event.target.value,errorTxt:'',numberList:list});
           if(event.target.value > 20){
               let totalPageArray= Math.ceil(list.length/20);
                this.setState({
                  showButton: true,
                  totalPages:totalPageArray,
                  splitArray: list.slice(0,20)});
            }
           else{
              this.setState({splitArray:list});
           }
      }
      else{
        this.setState({errorTxt: "Kindly enter values between 1 to 1000 only ",inputValue:'',numberList:'',splitArray:'',index:1,showButton:false});
      }
    };
    
    clearAllValues = () =>{
      this.setState({inputValue:'',numberList:'',splitArray:'',index:1,showButton:false});
    };
    
    getWord = () =>{
      const {inputValue,dayName} = this.state;
      if( (inputValue % 3 === 0) && (inputValue % 5 === 0) ){
        return (dayName =="Sat") ? 
        <p >Wizz Wuzz</p>
        :
        <p>Fizz Buzz </p>;
      }
      else if( inputValue % 3 === 0){
        return (dayName =="Sat") ? 
        <p style={{color:'blue'}}>Wizz</p>
        :
        <p style={{color:'blue'}}>Fizz</p>;
      }
      else if( inputValue % 5 === 0){
        return (dayName =="Sat") ? 
        <p style={{color:'green'}}>Wuzz</p>
        :
        <p style={{color:'green'}}>Buzz</p>;
      }
      else{
        return ;
      }
    };
    
    prevValues=(e) =>{
       const {numberList} = this.state;
       let pageValue= this.state.index; 
       if(pageValue>1){
          this.setState({index : pageValue-1});
          this.setState({splitArray : numberList.slice((pageValue-2)*20,(pageValue-1)*20)});
       }
       else{
          console.log("No Paging");
       }
    }
    
    nextValues=(e) =>{
      const {numberList} = this.state;
       let pageValue= this.state.index;
       this.setState({index : pageValue+1,
                      splitArray: numberList.slice(pageValue*20,(pageValue+1)*20) });
    }
    
      render() {
        return (
          <div className="Task">
            <div className="inputField">
              <label> Enter Required Number</label>
              <input type="number" value={this.state.inputValue} min={0} max={1000} onChange={this.inputChange}/>
              <input type="button" value="Clear"  onClick={this.clearAllValues} style={{marginLeft:'2%'}}/>
                {(this.state.errorTxt)&&(
                  <label className="errorLabel"> {this.state.errorTxt} </label> 
                )}
            </div>
    
            {(this.state.inputValue!='')&& (
              <div> {this.getWord()} </div>
            )}
    
             { (this.state.showButton ) && (
                <div>
                  <input type="button" value="Previous"  onClick={(e) => this.prevValues(e)} style={{marginLeft:'2%'}}
                   disabled={(this.state.index ===  1) ? true : false} />
                  <input type="button" value="Next"  onClick={(e) =>this.nextValues(e)} style={{marginLeft:'2%'}}
                    disabled={(this.state.index ===  this.state.totalPages) ? true : false}
                  />
                  <p>{this.state.index} out of {this.state.totalPages} </p> 
                </div>
             )}
              { (this.state.splitArray.length> 0) && (
                  <div className="showNumbers"> 
                    {this.state.splitArray.map(function(item,i){
                        return (
                          <p key={i}>
                            {item}
                          </p>
                          );
                    })}
                  </div>
              )}
    
    
    
          </div>
        );
      }
}
export default Task;