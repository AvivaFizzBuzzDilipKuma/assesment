import React, { Component } from 'react';
class TaskList extends Component {
    render() {
        return (
            <div>
                <h1>Features</h1>
                <div>
                       <p> The user should be able to enter a positive integer.</p>
                       <p> You should print out, in a vertical list, all of the values between 1 and the entered value.</p>
                       <p> Where the number is divisible by 3, you should instead print ‘fizz’.</p>
                       <p> Where the number is divisible by 5, you should instead print 'buzz'.</p>
                       <p> Where the number is divisible by 3 AND 5, you should instead print ‘fizz buzz’.</p>
                       <p> Add validation to ensure the entered value is an integer between 1 and 1000.</p>
                       <p> Add styling so 'fizz' is printed in blue, and 'buzz' in green</p>
                       <p> Change the logic so that if today is Wednesday, the words 'wizz' (in blue) and ‘wuzz' (in green) are substituted for 'fizz' and 'buzz </p> 
                       <p>Amend the application so only 20 values are displayed at a time. Implement ‘Next’ and ‘Previous’ buttons to display the remaining values.</p>
                </div>                         
            </div>
        );
    }
}
export default TaskList;